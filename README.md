# Goal and scope

This repo aim to develop one or more dockers for testing ansible playbooks.

At maturity, one should be able to use this repo to test a playbook.

# Usage

## Démarrer, arrêter, redémarrer ou détruire une machine.

    ./control start|stop|restart|kill


## Première connection

    cat hosts |grep =| sed 's/=/ /g' | awk '{printf("ssh %s@%s -p %s exit\n", $7, $3, $9)}' | xargs -I % -n 1 sh -c %

## Accéder manuellement à une machine

    ssh root@0.0.0.0 -p 32774

## Exécuter le playbook de test

    ansible-playbook dev.yaml


# Supported OS

## Ubuntu 16.04

An ubuntu 16.04 docker with ssh and python. Can only be accessed by user:password. Configuration hosts file example:




